let gameStarted = false 
let currentPlayer = 0 
let board = [] 


function start() {
    if (gameStarted == true) 
    return false 
    gameStarted = true 
    
    for (row = 0; row <= 5; row++) {
        board[row] = []
        for (col=0; col<=6; col++) {
            board[row][col] = 0
        }	
    }	

    drawBoard() 				
    currentPlayer = 1 
    nextTurn()

}

function drawBoard() {
    isGameWon() 
    for (col = 0; col <= 6; col++) {
        for (row = 0; row <= 5; row++) {
            document.getElementById('emptyTile'+row+'_'+col).innerHTML ="<span class='piece player"+board[row][col]+"'> </span>";
        }	
    }
}

function isGameWon() {
       for (i = 1; i <= 2; i++) {
            for (col = 0; col <= 3; col++) {
                for (row = 0; row <= 5; row++) {
                    if (board[row][col] == i) {
                    if ((board[row][col+1] == i) && (board[row][col+2] == i) && (board[row][col+3] == i)) {
                        endGame(i)
                        return true 
                    }
                }
            }
        }
    }
    
    for (i = 1; i <= 2; i++) {
        for (col = 0; col <= 6; col++) {
            for (row = 0; row <= 2; row++) {
                if (board[row][col] == i) {
                if ((board[row+1][col] == i) && (board[row+2][col] == i) && (board[row+3][col] == i)) {
                        endGame(i)
                        return true
                    }
                }
            }
        }
    }
    
    for (i = 1; i <= 2; i++) {
        for (col = 0; col <= 3; col++) {
            for (row = 0; row <= 2; row++) {
                if (board[row][col] == i) {
                if ((board[row+1][col+1] == i) && (board[row+2][col+2] == i) && (board[row+3][col+3] == i)) {
                        endGame(i)
                        return true
                    }
                }
            }
        }
    }
                    
    for (i = 1; i <= 2; i++) {
        for (col = 0; col <= 3; col++) {
            for (row = 3; row <= 5; row++) {
                if (board[row][col] == i) {
                if ((board[row-1][col+1] == i) && (board[row-2][col+2] == i) && (board[row-3][col+3] == i)) {
                        endGame(i)
                        return true
                    }
                }
            }
        }
    }
}

function endGame(winningPlayer) {
    gameStarted = false
    document.getElementById('displayWinner').innerHTML = "player " + winningPlayer + " wins!";   
}


function nextTurn() {
    if (gameStarted) { 
        document.getElementById('displayWinner').innerHTML = " <span class='player"+currentPlayer+"'></span>";
    }
}			


function playPiece(col) {
       for (row=5; row>=0; row--) { 
            if (board[row][col] == 0) {
                board[row][col] = currentPlayer;
                drawBoard(); 
               
                if (currentPlayer == 1) {
                    currentPlayer = 2
                } else {
                    currentPlayer = 1
                }
                          
                nextTurn(); 
                return true;
            }
        }
}